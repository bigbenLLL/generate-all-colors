import { RouteComponentProps, useHistory } from 'react-router-dom';
import React, { useEffect, useRef, useState } from 'react';
import { Button, Slider } from '@mui/material';
import { drawOnCanvas } from "../../utils/ImageDrawer";
import { SHUFFLE_TYPE } from "../../models/shuffle";
import './outcomeView.scss';

export const OutcomeView: React.FC<RouteComponentProps<void>> = () => {
  const canvasRef = useRef(null);
  const history = useHistory();
  const [shuffleSate, setShuffleState] = useState<SHUFFLE_TYPE>(SHUFFLE_TYPE.DEFAULT)
  const [threshold, setThreshold] = useState<number>(0.5)
  const handleOnclickHome = () => {
    history.push('/')
  }
  const handleOnclickUniform = () => {
    setShuffleState(SHUFFLE_TYPE.UNIFORM)
  }
  const handleOnclickNormal = () => {
    setShuffleState(SHUFFLE_TYPE.NORMAL)
  }
  const handleOnclickDefault = () => {
    setShuffleState(SHUFFLE_TYPE.DEFAULT)
  }
  const handleOnclickArtistic = () => {
    setShuffleState(SHUFFLE_TYPE.ARTISTIC)
  }
  const handleOnclickPoly = () => {
    setShuffleState(SHUFFLE_TYPE.POLY)
  }
  const handleChange = (e: any) => {
    setThreshold(e.target.value)
  }
  useEffect(() => {
    const canvas: any = canvasRef.current;
    const context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
    let animationFrameId: number;

    //Our draw came here
    const render = () => {
      drawOnCanvas(context, shuffleSate, threshold);
    };
    render();
    return () => {
      window.cancelAnimationFrame(animationFrameId);
    };
  }, [shuffleSate, threshold]);
  return (
    <div>
      <h2>Image Display</h2>
      <div className='content'>
        <div className='content__button-group'>
          <Button variant="outlined" onClick={() => {
            handleOnclickHome();
          }}>Home</Button>
          <Button variant="contained" onClick={() => {
            handleOnclickDefault();
          }}>Default</Button>
          <Button variant="contained" onClick={() => {
            handleOnclickUniform();
          }}>Uniform</Button>
          <Button variant="contained" onClick={() => {
            handleOnclickNormal();
          }}>Normal</Button>
          <Button variant="contained" onClick={() => {
            handleOnclickArtistic();
          }}>Artistic</Button>
          <Button variant="contained" onClick={() => {
            handleOnclickPoly();
          }}>Polynomial</Button>
        </div>
        <div className='content__threshold'>
          <Slider
            aria-label="Small steps"
            defaultValue={threshold}
            step={0.1}
            value={threshold}
            onChange={(e) => handleChange(e)}
            marks
            min={0}
            max={1}
            valueLabelDisplay="auto"
          />
        </div>

        <canvas ref={canvasRef} width="1024" height="600" />;
      </div>
    </div>
  )
};