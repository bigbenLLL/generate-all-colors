import { Button } from '@mui/material';
import React from 'react';
import { RouteComponentProps, useHistory } from 'react-router-dom';

export const RootView: React.FC<RouteComponentProps<void>> = () => {
    const history = useHistory();
    const handleOnclick = () => {
        history.push('/main')
    }
    return (
        <div>
            <header className='header-container'>
                <h1>Welcome to generate random color image!</h1>
            </header>
            <div className='content'>
                <Button
                    variant="contained"
                    onClick={() => {
                        handleOnclick();
                    }}
                >
                    Get Start
                </Button>
            </div>
        </div>
    );
};
