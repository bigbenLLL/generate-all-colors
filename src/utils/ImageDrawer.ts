import { SHUFFLE_TYPE } from "../models/shuffle";
//Generate a normal distribution number
export const random_nd = (): number => {
  let u = 0,
    v = 0;
  while (u === 0) u = Math.random();
  while (v === 0) v = Math.random();
  let num = Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v);
  num = num / 10.0 + 0.5;
  if (num > 1 || num < 0) return random_nd();
  return num;
};

class Rect {
  rgb: string;
  position: number[];
  constructor(rgb: string, position: number[]) {
    this.rgb = rgb;
    this.position = position;
  }
  getRgb = () => this.rgb;
  getPosition = () => this.position;
}

const getAllColorsOptions = (): string[] => {
  const steps: number[] = [];
  for (let i = 0; i < 256; i = i + 8) {
    steps.push(i);
  }
  const allColorsOptions: string[] = [];

  for (let i = 0; i < steps.length; i++) {
    for (let j = 0; j < steps.length; j++) {
      for (let k = 0; k < steps.length; k++) {
        allColorsOptions.push(`rgb(${steps[k]},${steps[j]},${steps[i]})`);
      }
    }
  }
  return allColorsOptions;
};

export const drawOnCanvas = (
  ctx: CanvasRenderingContext2D,
  shuffle: SHUFFLE_TYPE,
  threshold: number
) => {
  let allColors = getAllColorsOptions();
  let allColorsArr: number[][] | string[] = [...allColors];
  allColorsArr = allColorsArr.map((item: string, index: number) => {
    const left = item.indexOf("(");
    const right = item.indexOf(")");
    const colorStr = item.substring(left + 1, right);
    return colorStr.split(",").map((item: string) => parseInt(item));
  });
  switch (shuffle) {
    case SHUFFLE_TYPE.NORMAL:
      allColors.sort(() => {
        return threshold - random_nd();
      });
      break;
    case SHUFFLE_TYPE.UNIFORM:
      allColors.sort(() => {
        return threshold - Math.random();
      });
      break;
    case SHUFFLE_TYPE.ARTISTIC:
      allColorsArr.sort((a: number[], b: number[]) => sum(a) - sum(b));
      allColorsArr.sort(
        (a: number[], b: number[]) =>
          artCalculate(a, threshold) - artCalculate(b, threshold)
      );
      allColors = allColorsArr.map(
        (item: number[]) => `rgb(${item[0]},${item[1]},${item[2]})`
      );
      break;
    case SHUFFLE_TYPE.POLY:
      allColorsArr.sort(
        (a: number[], b: number[]) =>
          polyCalculate(a, threshold) - polyCalculate(b, threshold)
      );
      allColors = allColorsArr.map(
        (item: number[]) => `rgb(${item[0]},${item[1]},${item[2]})`
      );
      break;
    default:
      break;
  }
  const zoom = 4;
  for (let i = 0; i < 128; i++) {
    for (let j = 0; j < 256; j++) {
      const position = [j * zoom, i * zoom, zoom, zoom];
      const rect = new Rect(allColors[i * 256 + j], position);
      drawRect(ctx, rect);
    }
  }
};

const drawRect = (ctx: any, rect: Rect) => {
  ctx.fillStyle = rect.getRgb();
  ctx.fillRect(...rect.getPosition());
};

const sum = (arr: number[]) => {
  return arr.reduce(function (prev, curr, idx, arr) {
    return prev + curr;
  });
};

const artCalculate = (arr: number[], threshold: number) => {
  return (
    arr[(10 * threshold) % 2] -
    arr[(10 * threshold) % 3] -
    arr[(10 * threshold) % 4]
  );
};

const polyCalculate = (arr: number[], threshold: number) => {
  return arr[0] - (threshold * arr[1]^2)- (threshold * arr[2]^3);
};
