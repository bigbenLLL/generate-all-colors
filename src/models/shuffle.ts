export enum SHUFFLE_TYPE {
  DEFAULT = 'Default',
  UNIFORM = 'Uniform',
  NORMAL = 'Normal',
  ARTISTIC = 'Artistic',
  POLY = 'Polynomial',
}