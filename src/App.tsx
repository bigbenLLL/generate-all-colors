import React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import routes from "./router";
import { OutcomeView } from "./views/OutcomeView/OutcomeView";
import { RootView } from './views/RootView/RootView';

import './sass/App.scss';
function App() {
  return (
    <div className="App">
      <HashRouter>
        <div>
          <Switch>
            <Route path={routes.ROOT} exact component={RootView} />
            <Route path={routes.MAIN_VIEW} exact component={OutcomeView} />
          </Switch>
        </div>
      </HashRouter>
    </div>
  );
}

export default App;
